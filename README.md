#using directory:
   
   For using directory add "profile-image = src" attribute to img
   In CSS img[ profile-image ]  set back-image in attribute background-image
          
#custom classes:
    
        - profile-image-lg
       
        - profile-image-sm
       
        - profile-image-xs


#directory examples:

      <img profile-image="image.png" class="profile-image-lg"/>
